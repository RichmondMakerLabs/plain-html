# Aims

We recognise that in our society power is not held equally and that groups and 
individuals have been and continue to be discriminated against on many grounds 
including, for example, race, sex, age, disability, sexual orientation, class, 
religion, marital status and where they live or are from.

We also recognises that where direct or indirect discrimination occurs within 
our organization, it is both morally and legally unacceptable.

The purpose of the Equality and Diversity Policy is to set out clearly and 
fully the positive action we intend to take to combat direct and indirect 
discrimination in the organisation, in the services it provides and in its 
relationships with other bodies.

In adopting this Equality and Diversity Policy, we are also making an 
unequivocal commitment to implementing it, so as to ensure that equal 
opportunity becomes a reality.


# Code of Conduct

This code of conduct is not meant to cover all circumstances, 
please be understanding.

This code of conduct applies to your behaviour on all our communication 
channels, and any events that we run:

*    Our association is inclusive; do not engage in homophobic, racist, 
transphobic, ableist, xenophobic, sexist, or otherwise exclusionary behaviour. 
Do not make exclusionary jokes, even “ironically”.
*    Don’t harass people. Physical contact or sexual attention without 
enthusiastic consent is harassment, or if taken to its extreme, abuse. Wearing 
revealing clothes or flirting are not necessarily consent. If you’re asked to 
stop, do so. If you’re not sure the person is happy with what you are doing, 
ask.
*    Aggression and elitism are unwelcome – knowledge is not a competition.
*    We’d rather you ask about gender than assume, and if you get it wrong, 
apologise. Mistakes happen, however you should always use the person’s 
preferred pronouns when addressing or discussing them.
*    People’s private lives are their own. Do not share details about others 
that they have not explicitly made public. This includes, but is not limited to, 
sexuality, gender, medical conditions, housing, relationship or financial 
status.
*    Although alcoholic drinks are allowed, there is no expectation or pressure 
to drink alcohol or conversely not to drink alcohol. However, those who are 
obviously intoxicated will be asked to leave.
*    Discussion of how to make us more inclusive is welcome. Claims that this 
“has gone too far” aren’t.

Any breaches of the Code of Conduct shall be handled as mentioned below.

# Dealing with Complaints

The Founders will take complaints of discrimination and harassment very 
seriously. 

They will investigate them thoroughly, and provide opportunities for the person 
making the complaint to speak in a safe environment about their experience.

If the complaint is against a particular individual, the committee will hear 
their point of view.

The Committee will decide the action to take based on the principle of ensuring 
the continued inclusion and safety of any member who has experienced 
discrimination or harassment
